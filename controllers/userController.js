

const User = require("../models/User")

module.exports.getProfile = (userId) =>{
	return User.findById(userId).then(result => {
		if (result == null){
			return false;
		}
		else {
			result.password = "";
			return result;
		}
	})
}